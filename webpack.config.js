const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const isProd = process.env.NODE_ENV === 'production'

const MiniCssExtractPlugin = require('mini-css-extract-plugin')
module.exports = {
    // mode: 'production',
    entry: {
        SAEditor: path.resolve(__dirname, 'src/main.ts'),
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].min.js',
        library: 'SAEditor',
        libraryTarget: 'umd',
        libraryExport: 'default',
    },

   devtool: isProd ? false : 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/i,
                use: [
                    // 在开发过程中回退到 style-loader
                    isProd ? MiniCssExtractPlugin.loader : 'style-loader',
                    'css-loader',
                    'sass-loader',
                ],
            },
            {
                test: /\.ts$/,
                use: ['babel-loader', 'ts-loader'],
                include: /src/,
            },
        ],
    },
    resolve: {
        extensions: ['.ts', '.js'],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'public/index.html'),
        }),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(process.env.NODE_ENV),
            },
        }),

        new MiniCssExtractPlugin()
    ],
    devServer: {
        port: 8080,
        host: 'localhost',
        hot: true,
        open: true,
    },
}
