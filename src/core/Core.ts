import SAEditor from '../editor'
import { EditorConfig } from '../interface'

export default class Core {
    private editor: SAEditor
    constructor(editor: SAEditor, editorArea: HTMLElement, config: EditorConfig) {
        this.editor = editor
    }
    public init() {
        this.editor.beforeDestroy(function () {
            console.log('销毁core')
        })
    }
}
