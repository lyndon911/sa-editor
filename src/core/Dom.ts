class DomElement {
    private node: HTMLElement
    constructor(args: any) {
        this.node = this._createDomElement(args)[0]
    }

    private _createDomElement(strHtml: string): HTMLElement[] {
        let _parent = document.createElement('div')
        _parent.innerHTML = strHtml
        return Array.prototype.slice.call(_parent.children)
    }

    public getNode(): HTMLElement {
        return this.node
    }

    public appendChild(child: HTMLElement | DomElement): DomElement {
        if (child instanceof DomElement) {
            this.node.appendChild(child.getNode())
        } else {
            this.node.appendChild(child)
        }
        return this
    }
}

function $(arg): DomElement {
    return new DomElement(arg)
}
export default $

export { DomElement }
