import SAEditor from '../editor'

import $, {DomElement} from '../core/Dom'
import PluginList from './list'

export default class Plugins {
    public $toolbar: DomElement
    private readonly editor: SAEditor

    constructor(editor: SAEditor) {
        this.$toolbar = $(`<div class="edit__toolbars"></div>`)
        this.editor = editor
        this.initPlugin()
    }

    private initPlugin(): void {
        const keys: string[] = Object.keys(PluginList)
        this.editor.config.toolbars.forEach((item: string) => {
            if (item === '|') {
                this.$toolbar.appendChild($(`<div class="toolbar-box-separator"></div>`))
            } else if (keys.includes(item)) {
                const Plugin = PluginList[item]
                const _plugin = new Plugin(this.editor)
                this.compileElement(_plugin)
            }
        })
    }

    compileElement(plugin): void {
        const node: DomElement = plugin.$html
        this.compile(node, plugin)
    }

    compile(node, plugin) {
        this.$toolbar.appendChild(node)
    }

}
