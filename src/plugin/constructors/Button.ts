import $, { DomElement } from '../../core/Dom'
import SAEditor from '../../editor'
import Plugins from './Plugins'

export default class Button extends Plugins {

    constructor(editor: SAEditor, icon: DomElement) {
        super(editor)
        let _wrap = $(`<div class="toolbar-box toolbar-box-btn"></div>`)
        this.$html = _wrap.appendChild(icon)
    }
}
