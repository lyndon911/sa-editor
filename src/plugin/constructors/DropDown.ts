import Plugins from "./Plugins";
import SAEditor from "../../editor";
import $, {DomElement} from "../../core/Dom";

export default class DropDown extends Plugins {

    constructor(editor: SAEditor, icon: DomElement, panel: DomElement) {
        super(editor)
        let _wrap = $(`<div class="toolbar-box toolbar-box-dropdown"></div>`)
        this.$html = _wrap.appendChild(icon).appendChild(panel)
    }
}