import Bold from './bold'
import Italic from './italic'
import FullScreen from './fullscreen/fullScreen'

export default {
    bold: Bold,
    italic: Italic,
    fullscreen: FullScreen,
}
