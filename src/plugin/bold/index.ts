
import $ from '../../core/Dom'
import SAEditor from '../../editor'
import Button from '../constructors/Button'

export default class Bold extends Button{
    private editor: SAEditor
    public static key: string = 'bold'
    constructor(editor: SAEditor) {
        let svgIcon = $(`
        <svg class="icon" width="30px" height="30px" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <path d="M341.4 512V256c0-11.8 9.6-21.4 21.4-21.4h192c97.2 0 149.4 52.2 149.4 149.4 0 51.8-14.8 90.8-43.4 116 56.4 19 86.2 66.8 86.2 140 0 97.2-52.2 149.4-149.4 149.4H362.6c-11.8 0-21.4-9.6-21.4-21.4V512z" fill="#FFFFFF" />
            <path d="M341.4 277.4H320c-11.8 0-21.4-9.6-21.4-21.4 0-11.8 9.6-21.4 21.4-21.4h234.6c97.2 0 149.4 52.2 149.4 149.4 0 51.8-14.8 90.8-43.4 116 56.4 19 86.2 66.8 86.2 140 0 97.2-52.2 149.4-149.4 149.4H320c-11.8 0-21.4-9.6-21.4-21.4s9.6-21.4 21.4-21.4h21.4V277.4z m42.6 0v213.4h170.6c73.6 0 106.6-33.2 106.6-106.6s-33.2-106.6-106.6-106.6H384z m0 256v213.4h213.4c73.6 0 106.6-33.2 106.6-106.6s-33.2-106.6-106.6-106.6H384z"/>
        </svg>`)

        super(editor, svgIcon)
        this.editor = editor
    }

    public eventHandler(): void {
        this.setState()
        let _selection = this.editor.getSelection()
        if (_selection.isSelectionEmpty()) {
            //空选区需要添加空标签
        } else {
            //将选中文字进行加粗处理
        }
    }

    public checkActive() {
        // TODO 循环判断是否存在特定的标签或样式
        //
    }
}
