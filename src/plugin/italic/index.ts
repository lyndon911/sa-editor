import $ from '../../core/Dom'
import SAEditor from '../../editor'
import Button from '../constructors/Button'

export default class Italic extends Button {
    constructor(editor: SAEditor) {
        const elem = $(`
        <svg class="icon" width="30px" height="30px" viewBox="0 0 1024 1024" xmlns="http://www.w3.org/2000/svg">
            <path d="M391.2 746.6l195.6-469.4H512c-11.8 0-21.4-9.6-21.4-21.4 0-11.8 9.6-21.4 21.4-21.4h213.4c11.8 0 21.4 9.6 21.4 21.4 0 11.8-9.6 21.4-21.4 21.4h-92.4l-195.6 469.4H512c11.8 0 21.4 9.6 21.4 21.4s-9.6 21.4-21.4 21.4H298.6c-11.8 0-21.4-9.6-21.4-21.4s9.6-21.4 21.4-21.4h92.6z"  />
        </svg>`)
        super(editor, elem)
    }
}
