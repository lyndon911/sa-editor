export default interface EditorConfig {
    // 编辑器实例容器节点对象
    el: HTMLElement
    // 编辑器内容区域高度
    height: number
    // 国际化
    lang: string

    toolbars?: string[]
}
