import {DomElement} from "../core/Dom";

export default interface IDropDown {

    value: string | number

    active?: boolean

    text?: string | number

    template?: DomElement
}