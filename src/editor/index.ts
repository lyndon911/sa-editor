import $, {DomElement} from '../core/Dom'
import SelectionAPI from '../core/SelectionAPI'
import {EditorConfig} from '../interface'
import Core from '../core/Core'
// import Blod from '../plugin/bold'
import Plugins from '../plugin'

export default class SAEditor {
    public key: string
    public config: EditorConfig

    private readonly selection: SelectionAPI

    private beforeDestroyHooks: Function[]

    constructor(config: EditorConfig) {
        this.key = 'SAEditor_id_' + Date.now()
        this.config = config
        this.selection = new SelectionAPI(this)
        this.beforeDestroyHooks = []
        this.init()
        console.log(this.key)
    }

    public getSelection() {
        return this.selection
    }

    /**
     * 编辑器的初始化方法
     */
    public init(): void {
        this.config.el.setAttribute('key', this.key)
        this.config.el.style.height = this.config.height + 'px'

        let editArea: HTMLElement = $(
            `<div class="edit__area" contenteditable="true" autofocus="autofocus"></div>`
        ).getNode()

        const plugin = new Plugins(this)

        this.config.el.appendChild(plugin.$toolbar.getNode())

        this.config.el.appendChild(editArea)

        // new Core(this, editArea, this.config)

        // let bold = new Blod(this);
        // bold.eventHandler()


    }

    /**
     *  组件销毁前的回调函数钩子
     * @param func 回调方法
     */
    public beforeDestroy(func: Function): void {
        this.beforeDestroyHooks.push(func)
    }

    /**
     * 销毁编辑器及注册的钩子函数
     */
    public destroy(): void {
        //销毁注册的回调事件
        this.beforeDestroyHooks.forEach(func => func.call(this))
        // 销毁编辑器实例
    }
}
